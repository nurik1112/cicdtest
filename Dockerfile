FROM ubuntu:18.04
RUN apt-get -y update
RUN apt-get install -y nginx
RUN mkdir /usr/src/nginxtest
COPY . /usr/src/nginxtest
RUN mkdir /site
WORKDIR /usr/src/nginxtest
RUN mv burger /site/
RUN mv nginx.conf /etc/nginx/
WORKDIR /
RUN chown -R nobody /site
WORKDIR /site/burger
RUN chmod -R ug+rwx css
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
CMD [ "nginx" ]
